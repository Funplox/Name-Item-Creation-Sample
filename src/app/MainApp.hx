package app;
import app.items.Floor;
import app.items.Hexagon;
import app.items.Ramp;
import app.items.Sexagon;
import app.items.Stone;
import app.napeWorld.World;
import app.napeWorld.WorldDebugDrawing;
import com.creativemage.tilesheet.EasyTilesheet;
import openfl.Assets;
import openfl.display.Sprite;
import openfl.events.Event;
import openfl.geom.Point;
import openfl.Lib;

/**
 * ...
 * @author Creative Magic
 */
class MainApp extends Sprite
{
	var c:Int = 0;
	
	private var world:World;
	private var tileSheet:EasyTilesheet;

	public function new() 
	{
		super();
		
		setTileSheet();
		setWorld();
		
		addEventListener(Event.ENTER_FRAME, onFrame);
	}
	
	function setTileSheet() 
	{
		tileSheet = new EasyTilesheet( this.graphics );
		tileSheet.addTextureToAtlas( Assets.getBitmapData("img/ground.jpg"));
		tileSheet.addTextureToAtlas( Assets.getBitmapData("img/stone.png"), new Point(20, 20));
		tileSheet.addTextureToAtlas( Assets.getBitmapData("img/Ramp.png"), new Point(0, 0) );
		tileSheet.addTextureToAtlas( Assets.getBitmapData("img/hexa.png"), new Point(50, 105 / 2) );
		tileSheet.addTextureToAtlas( Assets.getBitmapData("img/sexa.png"), new Point(75, 65) );
		tileSheet.init();
		
	}
	
	function setWorld() 
	{
		world = new World();
		world.init();
		
		var ground = new Floor();
		world.addBody(ground);
		tileSheet.addAnimationBody(ground.animation);
		ground.animation.y = 400;
		
		var ramp = new Ramp();
		world.addBody(ramp, 200, 200);
		tileSheet.addAnimationBody(ramp.animation);
		
		var hexa = new Hexagon();
		world.addBody(hexa, 550, 200);
		tileSheet.addAnimationBody(hexa.animation);
		
		var sexa = new Sexagon();
		world.addBody(sexa, 380, 100);
		tileSheet.addAnimationBody(sexa.animation);
		
		WorldDebugDrawing.setup( world.space);
		
	}
	
	private function createStone():Void
	{
		var posX:Float = Math.random() * Lib.current.stage.stageWidth;
		
		var s = new Stone( Std.random(15) + 10 );
		world.addBody( s, posX, 0);
		
		tileSheet.addAnimationBody(s.animation);
		s.animation.x = posX;
		s.animation.y = 0;
	}
	
	// EVENT HANDLERS
	
	private function onFrame(e:Event):Void 
	{
		if (c % 45 == 0 && c < 10000)
		createStone();
		
		world.update();
		tileSheet.update();
		
		c++;
		
		WorldDebugDrawing.update();
	}
	
}