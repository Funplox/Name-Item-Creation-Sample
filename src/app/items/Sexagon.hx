package app.items;
import app.napeWorld.WorldItem;
import com.creativemage.tilesheet.AnimatedBody;
import com.creativemage.tilesheet.Animation;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;

/**
 * ...
 * @author Alex Kolpakov
 */
class Sexagon extends WorldItem
{

	public function new() 
	{
		super();
		
		setBody();
		setAnimation();
	}
	
	function setBody() 
	{
		body = new Body( BodyType.DYNAMIC);
		body.shapes.add( new Polygon(Polygon.regular( 75, 75, 6)));
	}
	
	function setAnimation() 
	{
		var a:Animation = new Animation([4]);
		
		animation = new AnimatedBody();
		animation.addAnimation(a);
	}
	
}