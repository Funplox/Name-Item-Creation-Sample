package app.items;
import app.napeWorld.WorldItem;
import com.creativemage.tilesheet.AnimatedBody;
import com.creativemage.tilesheet.Animation;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Circle;

/**
 * ...
 * @author Creative Magic
 */
class Stone extends WorldItem
{
	var radius:Float;

	public function new(r:Float) 
	{
		super();
		
		radius = r;
		
		setBody();
		setAnimation();
	}
	
	function setBody() 
	{
		body = new Body( BodyType.DYNAMIC);
		body.shapes.add( new Circle( 20 ));
		body.allowRotation = true;
	}
	
	function setAnimation() 
	{
		var a:Animation = new Animation([1]);
		
		animation = new AnimatedBody();
		animation.addAnimation(a);
	}
	
	override public function update():Void 
	{
		
	}
	
}