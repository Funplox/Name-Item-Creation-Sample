package app.items;
import app.napeWorld.WorldItem;
import com.creativemage.tilesheet.AnimatedBody;
import com.creativemage.tilesheet.Animation;
import nape.geom.Vec2;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.phys.Material;
import nape.shape.Polygon;

/**
 * ...
 * @author Alex Kolpakov
 */
class Ramp extends WorldItem
{

	public function new() 
	{
		super();
		
		setBody();
		setAnimation();
	}
	
	function setBody() 
	{
		body = new Body( BodyType.DYNAMIC);
		body.shapes.add( new Polygon([Vec2.weak(0, 80), Vec2.weak(200, 80), Vec2.weak(200, 0)] ));
		body.allowRotation = false;
	}
	
	function setAnimation() 
	{
		var a:Animation = new Animation([2]);
		
		animation = new AnimatedBody();
		animation.addAnimation(a);
	}
	
}