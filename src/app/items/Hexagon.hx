package app.items;
import app.napeWorld.WorldItem;
import com.creativemage.tilesheet.AnimatedBody;
import com.creativemage.tilesheet.Animation;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;

/**
 * ...
 * @author Alex Kolpakov
 */
class Hexagon extends WorldItem
{

	public function new() 
	{
		super();
		
		setBody();
		setAnimation();
	}
	
	function setBody() 
	{
		body = new Body( BodyType.DYNAMIC);
		body.shapes.add( new Polygon(Polygon.regular( 52, 54, 5)));
		body.allowRotation = false;
	}
	
	function setAnimation() 
	{
		var a:Animation = new Animation([3]);
		
		animation = new AnimatedBody();
		animation.addAnimation(a);
	}
	
}