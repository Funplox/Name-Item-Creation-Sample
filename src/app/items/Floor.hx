package app.items;
import app.napeWorld.WorldItem;
import com.creativemage.tilesheet.AnimatedBody;
import com.creativemage.tilesheet.Animation;
import nape.phys.Body;
import nape.phys.BodyType;
import nape.shape.Polygon;

/**
 * ...
 * @author Creative Magic
 */
class Floor extends WorldItem
{

	public function new() 
	{
		super();
		
		setBody();
		setAnimation();
	}
	
	function setBody() 
	{
		body = new Body( BodyType.STATIC);
		body.shapes.add( new Polygon( Polygon.rect( 0, 400, 800, 200)));
	}
	
	function setAnimation() 
	{
		var a:Animation = new Animation([0]);
		
		animation = new AnimatedBody();
		animation.addAnimation(a);
	}
	
}