package app.napeWorld ;
import nape.space.Space;
import nape.util.BitmapDebug;
import nape.util.Debug;
import openfl.Lib;

/**
 * ...
 * @author Alex Kolpakov
 */
class WorldDebugDrawing
{	
	
	private static var space:Space;
	#if flash
    private static var debug:Debug;
	#end
	public function new() 
	{
		
	}
	
	public static function setup(spaceRef:Space):Void
	{
		#if flash
		space = spaceRef;
		
		
		debug = new BitmapDebug(Lib.current.stage.stageWidth, Lib.current.stage.stageHeight, 0x00000000, true);
        Lib.current.stage.addChild(debug.display);
		#end
	}
	
	public static function update():Void
	{
		#if flash
		debug.clear();
        debug.draw(space);
        debug.flush();
		#end
	}
	
}