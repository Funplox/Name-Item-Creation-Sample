package app.napeWorld;
import nape.geom.Vec2;
import nape.phys.BodyType;
import nape.space.Space;

/**
 * ...
 * @author Creative Magic
 */
class World
{
	public static inline var TIME_OFFSET:Float = 1 / 60;

	
	public var space:Space;
	private var items:Array<WorldItem> = [];
	

	public function new() 
	{
		
	}
	
	public function init():Void
	{
		space = new Space( Vec2.weak( 0, 600));
	}
	
	public function addBody(item:WorldItem, ?positionX:Float, ?positionY:Float, ?rotationInRad:Float):Void
	{
		item.body.space = space;
		
		if (item.body.type == BodyType.STATIC)
		return;
		
		if (positionX != null)
		item.body.position.x = positionX;
		
		if (positionY != null)
		item.body.position.y = positionY;
		
		if (rotationInRad != null)
		item.body.rotation = rotationInRad;
		
		
		items.push(item);
	}
	
	public function update():Void
	{
		space.step( TIME_OFFSET );
		
		for ( i in items)
		{
			i.update();
			#if !flash
			i.animation.x = i.body.position.x;
			i.animation.y = i.body.position.y;
			
			i.animation.rotation = i.body.rotation;
			#else
			i.animation.x = i.body.position.x;
			i.animation.y = i.body.position.y;
			
			i.animation.rotation = Math.PI * .5 + i.body.rotation;
			#end
		}
	}
	
}