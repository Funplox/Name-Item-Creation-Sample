package com.creativemage.tilesheet;
import openfl.display.BitmapData;
import openfl.geom.Point;
import openfl.geom.Rectangle;

/**
 * ...
 * @author Creative Magic
 */
class TextureData
{
	public var texture:BitmapData;
	public var centerPoint:Point;
	public var rect:Rectangle;

	public function new() 
	{
		
	}
	
}